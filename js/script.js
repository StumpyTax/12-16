let button = document.querySelector('.burger__button');

let menu = document.querySelector('.burger');

button.addEventListener('click', () => {
  if (menu.classList.contains('burger--active')) {
    menu.classList.remove('burger--active');
  }
  else {
    menu.classList.toggle('burger--active');
  }
  button.classList.toggle('burger__button--open');
});

let menu_elemtnts = document.querySelectorAll('.header__element');

menu_elemtnts.forEach((i) => {
  i.addEventListener('click', () => {
    if (menu.classList.contains('burger--active')) {
      menu.classList.remove('burger--active');
    }
    if (button.classList.contains('burger__button--open')) {
      button.classList.remove('burger__button--open');
    }
  });
});

const swiper = new Swiper('.swiper', {
  slidesPerView: 1,
  direction: "horizontal",
  slidesPerGorup: 1,
  loop: true,

  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true,
  },
});

let step_numbers = document.querySelectorAll('.step-number');
let steps = document.querySelectorAll('.step');

step_numbers.forEach((e) => {
  e.addEventListener('click', (e) => {
    let data = e.currentTarget.dataset.path;

    step_numbers.forEach((e) => {
      if (e.classList.contains('step-number--active')) {
        e.classList.remove('step-number--active');
      }
    });

    steps.forEach((e) => {
      if (e.classList.contains('step--active')) {
        e.classList.remove('step--active');
      }

      document.querySelector(`[data-path="${data}"]`).classList.add('step-number--active');
      document.querySelector(`[data-target="${data}"]`).classList.add('step--active');
    });
  });
});

let buttons = document.querySelectorAll('.quest__button');

buttons.forEach((e) => {
  e.addEventListener('click', () => {
    if (e.classList.contains('quest__button--active')) {
      e.classList.remove('quest__button--active');
      e.parentElement.parentElement.children[1].classList.remove('quest__answer--active');
    }
    else {
      e.classList.add('quest__button--active');
      e.parentElement.parentElement.children[1].classList.add('quest__answer--active');
    }
  });
});

searchButton=document.querySelector('.search__button');
search=document.querySelector('.search');
searchClose=document.querySelector('.search__cancel');

searchButton.addEventListener('click',()=>{
  if(document.body.clientWidth<=768){
    document.querySelector('.header__logo').classList.add('__hide');
    if(document.body.clientWidth<=320)
    document.querySelector('.burger').classList.add('__hide');
  }
  search.classList.add('search--active');
});

searchClose.addEventListener('click',()=>{
  if(document.body.clientWidth<=768){
    document.querySelector('.header__logo').classList.remove('__hide');
    if(document.body.clientWidth<=320)
      document.querySelector('.burger').classList.remove('__hide');
  }
  search.classList.remove('search--active');
});